﻿namespace grybai
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.men = new System.Windows.Forms.TextBox();
            this.menuo = new System.Windows.Forms.Label();
            this.raud = new System.Windows.Forms.TextBox();
            this.mas = new System.Windows.Forms.TextBox();
            this.dien = new System.Windows.Forms.TextBox();
            this.bara = new System.Windows.Forms.TextBox();
            this.baravykai = new System.Windows.Forms.Label();
            this.raudon = new System.Windows.Forms.Label();
            this.mase = new System.Windows.Forms.Label();
            this.diena = new System.Windows.Forms.Label();
            this.issaugoti = new System.Windows.Forms.Button();
            this.derling = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // men
            // 
            this.men.Location = new System.Drawing.Point(114, 28);
            this.men.Multiline = true;
            this.men.Name = "men";
            this.men.Size = new System.Drawing.Size(145, 24);
            this.men.TabIndex = 0;
            // 
            // menuo
            // 
            this.menuo.AutoSize = true;
            this.menuo.Location = new System.Drawing.Point(34, 31);
            this.menuo.Name = "menuo";
            this.menuo.Size = new System.Drawing.Size(43, 13);
            this.menuo.TabIndex = 1;
            this.menuo.Text = "Menuo:";
            // 
            // raud
            // 
            this.raud.Location = new System.Drawing.Point(114, 167);
            this.raud.Multiline = true;
            this.raud.Name = "raud";
            this.raud.Size = new System.Drawing.Size(145, 24);
            this.raud.TabIndex = 2;
            // 
            // mas
            // 
            this.mas.Location = new System.Drawing.Point(114, 119);
            this.mas.Multiline = true;
            this.mas.Name = "mas";
            this.mas.Size = new System.Drawing.Size(145, 24);
            this.mas.TabIndex = 5;
            // 
            // dien
            // 
            this.dien.Location = new System.Drawing.Point(114, 71);
            this.dien.Multiline = true;
            this.dien.Name = "dien";
            this.dien.Size = new System.Drawing.Size(145, 24);
            this.dien.TabIndex = 6;
            // 
            // bara
            // 
            this.bara.Location = new System.Drawing.Point(114, 213);
            this.bara.Multiline = true;
            this.bara.Name = "bara";
            this.bara.Size = new System.Drawing.Size(145, 24);
            this.bara.TabIndex = 7;
            // 
            // baravykai
            // 
            this.baravykai.AutoSize = true;
            this.baravykai.Location = new System.Drawing.Point(34, 216);
            this.baravykai.Name = "baravykai";
            this.baravykai.Size = new System.Drawing.Size(57, 13);
            this.baravykai.TabIndex = 9;
            this.baravykai.Text = "Baravykai:";
            // 
            // raudon
            // 
            this.raudon.AutoSize = true;
            this.raudon.Location = new System.Drawing.Point(34, 170);
            this.raudon.Name = "raudon";
            this.raudon.Size = new System.Drawing.Size(74, 13);
            this.raudon.TabIndex = 10;
            this.raudon.Text = "Raudonvirsiai:";
            // 
            // mase
            // 
            this.mase.AutoSize = true;
            this.mase.Location = new System.Drawing.Point(34, 122);
            this.mase.Name = "mase";
            this.mase.Size = new System.Drawing.Size(36, 13);
            this.mase.TabIndex = 11;
            this.mase.Text = "Mase:";
            // 
            // diena
            // 
            this.diena.AutoSize = true;
            this.diena.Location = new System.Drawing.Point(34, 74);
            this.diena.Name = "diena";
            this.diena.Size = new System.Drawing.Size(38, 13);
            this.diena.TabIndex = 12;
            this.diena.Text = "Diena:";
            // 
            // issaugoti
            // 
            this.issaugoti.Location = new System.Drawing.Point(37, 270);
            this.issaugoti.Name = "issaugoti";
            this.issaugoti.Size = new System.Drawing.Size(145, 23);
            this.issaugoti.TabIndex = 13;
            this.issaugoti.Text = "Issaugoti";
            this.issaugoti.UseVisualStyleBackColor = true;
            this.issaugoti.Click += new System.EventHandler(this.issaugoti_Click);
            // 
            // derling
            // 
            this.derling.Location = new System.Drawing.Point(37, 313);
            this.derling.Name = "derling";
            this.derling.Size = new System.Drawing.Size(145, 23);
            this.derling.TabIndex = 14;
            this.derling.Text = "Rask derlingiausia diena";
            this.derling.UseVisualStyleBackColor = true;
            this.derling.Click += new System.EventHandler(this.derling_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(343, 270);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(132, 20);
            this.textBox1.TabIndex = 15;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(343, 313);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(132, 20);
            this.textBox2.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(296, 276);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Diena:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(296, 313);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Menuo:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 385);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.derling);
            this.Controls.Add(this.issaugoti);
            this.Controls.Add(this.diena);
            this.Controls.Add(this.mase);
            this.Controls.Add(this.raudon);
            this.Controls.Add(this.baravykai);
            this.Controls.Add(this.bara);
            this.Controls.Add(this.dien);
            this.Controls.Add(this.mas);
            this.Controls.Add(this.raud);
            this.Controls.Add(this.menuo);
            this.Controls.Add(this.men);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox men;
        private System.Windows.Forms.Label menuo;
        private System.Windows.Forms.TextBox raud;
        private System.Windows.Forms.TextBox mas;
        private System.Windows.Forms.TextBox dien;
        private System.Windows.Forms.TextBox bara;
        private System.Windows.Forms.Label baravykai;
        private System.Windows.Forms.Label raudon;
        private System.Windows.Forms.Label mase;
        private System.Windows.Forms.Label diena;
        private System.Windows.Forms.Button issaugoti;
        private System.Windows.Forms.Button derling;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

