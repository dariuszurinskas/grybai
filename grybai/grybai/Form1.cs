﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace grybai
{
    public partial class Form1 : Form
    {
        const string failas = "grybai.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private void issaugoti_Click(object sender, EventArgs e)
        {
            try
            {
                int menuo = Convert.ToInt32(men.Text);
                int diena = Convert.ToInt32(dien.Text);
                float mase = Convert.ToSingle(mas.Text);
                int raudonvirsiai = Convert.ToInt32(raud.Text);
                int baravykai = Convert.ToInt32(bara.Text);

                System.IO.File.AppendAllText(failas, menuo + " " + diena + " " + mase + " " + raudonvirsiai + " " + baravykai);
                System.IO.File.AppendAllText(failas, Environment.NewLine);
            }
            catch
            {
                MessageBox.Show("Neteisingai pateikti duomenys");

            }



        }

        private void derling_Click(object sender, EventArgs e)
        {
            string[] lines = System.IO.File.ReadAllLines(failas);

            int[] menuo = new int[lines.Length];
            int[] diena = new int[lines.Length];
            float[] mase = new float[lines.Length];
            int[] raudonvirsiai = new int[lines.Length];
            int[] baravykai = new int[lines.Length];

            for (int i = 0; i < lines.Length; i++)
            {
                string[] items = lines[i].Split(' ');

                menuo[i] = Convert.ToInt32(items[0]);
                diena[i] = Convert.ToInt32(items[1]);
                mase[i] = Convert.ToSingle(items[2]);
                raudonvirsiai[i] = Convert.ToInt32(items[3]);
                baravykai[i] = Convert.ToInt32(items[4]);

            }
            float did = mase[0];
            int didIndeskas = 0;
            for (int i = 0; i < mase.Length; i++)
            {
                if (mase[i] > did)

                {
                    did = mase[i];
                    didIndeskas = i;
                }
                
            }
            textBox2.Text = menuo[didIndeskas].ToString();
            textBox1.Text = diena[didIndeskas].ToString();
            
        }
    }
}



